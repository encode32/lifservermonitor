import sys
import os
import urllib
import urllib2
import cookielib
from time import sleep
import json

_author = 'encode'
_sleepsec = 30
_username = 'user'
_password = 'pass'

#Clear
def _clear():
    #windows
    os.system('cls')
    #linux
    #os.system('clear')

#Sleep
def _sleep():
    sleep(_sleepsec)

#Monitor
def _monitor():
    cj = cookielib.CookieJar()
    opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(cj))
    login_data = urllib.urlencode({'type' : 'plain',
                                   'usrlgn' : _username,
                                   'password' : _password})
    opener.open('https://lifeisfeudal.com/billing/service-signin.php', login_data)
    data = opener.open('https://lifeisfeudal.com/billing/service-status.php?action=getServersStatus')

    _clear()
    #Author
    print "=============================================================================="
    print " LiF Monitor | " + _author

    #Workers
    print "=============================================================================="
    apidata = json.load(data)

    if apidata['ok']:
        opened = apidata['data']['dispatcher']['IsOpen']
        if opened is 1:
            print " Server Dispatcher Opened"
        else:
            print " Server Dispatcher Closed"
        print "------------------------------------------------------------------------------"

        for i in range(51):

            try:
                server = apidata['data']['servers'][str(i)]
                print " ----> Server " + str(i) + " have " + server + " people connected."
            except KeyError:
                print " [X] Server " + str(i) + " down."
                pass
    else:
        print " Error: Loging"
    print "------------------------------------------------------------------------------"


#Init
while 1:
    _monitor()
    _sleep()